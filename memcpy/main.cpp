void * memcpy(void *dest, void const *src, std::size_t n)
{
    auto d = static_cast<std::byte *>(dest);
    auto s = static_cast<std::byte const *>(src);

    auto last = s + n;

    while(s != last)
    {
        *d = *s;
        s++;
        d++;
    }

    return dest;
}

struct Object
{
    int x = 123;
    int y = 456;
    int z = 789;
};

int main()
{
    Object object;
    int data[3];

    memcpy(data, &object, sizeof(object));

    std::cout << data[0] << data[1] << data[2] << std::endl;

    return EXIT_SUCCESS;
}
