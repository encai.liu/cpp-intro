struct Coordinates
{
    float x;
    float y;
    float z;
};

int main()
{
    std::vector<int> v;
    std::vector<int> w = {1, 2, 3, 4, 5};

    std::cout << "v.size() before move: " << v.size() << std::endl; 
    std::cout << "w.size() before move: " << w.size() << std::endl; 

    // move of std::vector
    v = std::move(w);

    std::cout << "v.size() after move: " << v.size() << std::endl; 
    std::for_each( std::begin(v), std::end(v),
        []( auto x ){ std::cout << x << ", "; } );  // 1, 2, 3, 4, 5
    std::cout << std::endl; 

    std::cout << "w.size() after move: " << w.size() << std::endl; 
    std::for_each( std::begin(w), std::end(w),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // resize
    w.resize(3);
    for(auto i = 0; i != 3; ++i){
        w[i] = i;
    }
    std::cout << "w.size() after resize: " << w.size() << std::endl; 
    std::for_each( std::begin(w), std::end(w),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // move of int
    int a = 10;
    int c = std::move(a);

    std::cout << "a: " << a << std::endl;
    std::cout << "c( c = std::move(a) ): " << c << std::endl;

    // move of class/struct
    Coordinates p1{1, 2, 3};
    Coordinates p2 = std::move( p1 );
    std::cout << "p1.x: " << p1.x << std::endl;
    std::cout << "p1.y: " << p1.y << std::endl;
    std::cout << "p1.z: " << p1.z << std::endl;
    std::cout << "p2.x: " << p2.x << std::endl;
    std::cout << "p2.y: " << p2.y << std::endl;
    std::cout << "p2.z: " << p2.z << std::endl;

    return EXIT_SUCCESS;
}
