template <typename Engine>
auto try_coinflips( Engine &e)
{
    std::geometric_distribution d(0.5);
    return d(e) + 1;
}

template <typename Engine>
auto try_rolls( Engine &e)
{
    std::geometric_distribution d(1.0/6.0);
    return d(e) + 1;
}

template <typename Engine>
auto try_lootboxes( Engine &e)
{
    std::geometric_distribution d(1.0/100.0);
    return d(e) + 1;
}

int main()
{
    std::mt19937 e;

    for(auto i=0; i!=10; ++i){
        std::cout << try_coinflips(e) << " "sv;
    }
    std::cout << std::endl;

    for(auto i=0; i!=10; ++i){
        std::cout << try_rolls(e) << " "sv;
    }
    std::cout << std::endl;

    for(auto i=0; i!=10; ++i){
        std::cout << try_lootboxes(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}

