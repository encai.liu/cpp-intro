#include <iostream>

inline std::string delimiter{"\n"}; // must be compiled with "-std=c++17"

inline void print_int( int x )
{
    std::cout << x << delimiter;
}

