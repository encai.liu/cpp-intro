int main()
{
    std::cout << "sizeof(int8_t):    "s << sizeof(int8_t)      << " byte(s), "s
              << "min: "s << +std::numeric_limits<int8_t>::min() 
              << ", max: "s << +std::numeric_limits<int8_t>::max() << "\n"s;
    std::cout << "sizeof(char):      "s << sizeof(char)      << " byte(s), "s
              << "min: "s << +std::numeric_limits<char>::min() 
              << ", max: "s << +std::numeric_limits<char>::max() << "\n"s;
    std::cout << "sizeof(short):     " << sizeof(short)     << " byte(s), "
              << "min: "s << std::numeric_limits<short>::min() 
              << ", max: "s << std::numeric_limits<short>::max() << "\n"s;
    std::cout << "sizeof(int):       " << sizeof(int)       << " byte(s), "
              << "min: "s << std::numeric_limits<int>::min() 
              << ", max: "s << std::numeric_limits<int>::max() << "\n"s;
    std::cout << "sizeof(long):      " << sizeof(long)      << " byte(s), "
              << "min: "s << std::numeric_limits<long>::min() 
              << ", max: "s << std::numeric_limits<long>::max() << "\n"s;
    std::cout << "sizeof(long long): " << sizeof(long long) << " byte(s), "
              << "min: "s << std::numeric_limits<long long>::min() 
              << ", max: "s << std::numeric_limits<long long>::max() << "\n"s;
 
    return EXIT_SUCCESS;
}
