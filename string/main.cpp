void print_byte( std::byte x )
{
    std::cout << static_cast<unsigned int>(x);
}

int main()
{
    std::cout << "こんにちは"s << std::endl;

    std::uint16_t value = 0b00000001'00000010;
    std::byte rep[2];

    std::memcpy( rep, &value, 2 );

    print_byte( rep[0] );   // 21 in little-endian systems <- ideapad duet
    print_byte( rep[1] );   // 12 in big-endian systems
    std::cout << std::endl;

    auto s = "abc" "def";   // -> "abcdef
    std::cout << "s = " << s << std::endl;

    int8_t s1[] = u8"いろは";  // UTF8
    char16_t s2[] = u"いろは"; // UTF16
    char32_t s3[] = U"いろは"; // UTF32
    std::cout << "s1 = "s << s1 << std::endl;
    std::cout << "s2 = "s << s2 << std::endl;
    std::cout << "s3 = "s << s3 << std::endl;

    std::cout << "\\n は改行文字\n"s;
    std::cout << "\' は単一引用符\n"s;
    std::cout << "\" は二重引用符\n"s;

    // raw string literal
    auto rstr = R"(
'は単一引用符
"二重は引用符
\n は改行文字
)";
    std::cout << "raw str = " << rstr;

    auto size = std::strlen(rstr); 
    std::cout << "size of rstr: "s << size << std::endl;

    // null terminated
    char s4[] = "abc";
    std::cout << "sizeof(s4): "s << std::strlen(s4) << "\n"s;  // 3
    s4[1] = '\0'; // null
    std::cout << "sizeof(s4): "s << std::strlen(s4) << "\n";  // 1

    // concatenate strings
    char s5[] = "abcd";
    char s6[] = "efgh";
    size = std::strlen(s5) + std::strlen(s6) - 1;
    char *s7 = new char[size];

    std::strcpy(s7, s5);
    std::strcat(s7, s6);
    std::cout << "Concatenated string: "s << s7 << "\n";  // abcdefgh

    std::string s8 = s5;
    s8 += s6;
    std::cout << "Concatenated string: "s << s8 << "\n";  // abcdefgh

    auto s9 = "abcd"s + "efgh"s;
    std::cout << "Concatenated string using +: "s << s9 << "\n";  // abcdefgh

    auto s10 = "abcd"s;
    s10 += "efgh"s;
    std::cout << "Concatenated string using +=: "s << s10 << "\n";  // abcdefgh

    auto s11 = "abcd"s;
    s11.append("efgh"s);
    std::cout << "Concatenated string using append: "s << s11 << "\n";  // abcdefgh

    // iterator
    {
        auto s = "hello"s;
        for( auto i = s.begin(); i != s.end(); ++i){
            std::cout << *i;
        }
        std::cout << "\n"s;

        for( auto i = std::begin(s); i != std::end(s); ++i){
            std::cout << *i;
        }
        std::cout << "\n"s;
    }

    // search
    {
        auto text = "quick brown fox jumps over the lazy dog."s;

        auto word = "fox"s;
        auto i = std::search( text.begin(), text.end(), word.begin(), word.end() );
        if( i != text.end() )
            std::cout << "fox found\n"s;
        else
            std::cout << "no fox...\n"s;

        word = "foxs"s;
        i = std::search( text.begin(), text.end(), word.begin(), word.end() );
        if( i != text.end() )
            std::cout << "foxs found\n"s;
        else
            std::cout << "no foxs...\n"s;

        auto fox = text.find("fox"sv);
        auto dog = text.find("dog"sv);
        std::cout << "fox: "s << fox << ", dog: "s << dog << std::endl;

        auto index = text.find("abc"sv);
        if( index != std::string::npos )
            std::cout << "abc found\n"s;
        else
            std::cout << "no abc...\n"s;

        index = text.find("lazy dog"sv);
        if( index != std::string::npos )
            std::cout << "lazy dog found\n"s;
        else
            std::cout << "no lazy dog...\n"s;

        text  = "word word word"s;
        auto first = text.find("word");
        auto last  = text.rfind("word");
        std::cout << "first: "s << first << ", last: " << last << std::endl;
    }

    // insert
    {
        auto text = "cat"s;
        text.insert( 0, "long "sv );
        text.insert( text.size(), " is loong."sv);
        std::cout << text << std::endl;
    }
    {
        auto text = "big cat"s;
        text.insert( text.find("cat"sv), "fat "sv);
        std::cout << text << std::endl;
    }

    // erase
    {
        auto text = "dirty cat"s;
        auto dirty = "dirty "s;
        text.erase( 0, dirty.size());
        std::cout << text << std::endl;
    }
    {
        auto text = "big fat cat"s;
        auto fat = "fat "s;
        text.erase( text.find(fat), fat.size() );
        std::cout << text << std::endl;
    }

    // replace
    {
        auto text = "ugly cat"s;
        auto ugly = "ugly"sv;
        auto pretty = "pretty"sv;

        text.replace( text.find(ugly), ugly.size(), pretty );
        std::cout << text << std::endl;
    }

    // basic_string_view
    {
        auto text = "quick brown fox jump over the lazy dog."s;
        std::cout << text << std::endl;
        // text.remove_prefix( "quick "sv.size() );
        text.erase( 0, "quick "sv.size() );
        std::cout << text << std::endl;
        text.erase( 0, "brown "sv.size() );
        std::cout << text << std::endl;
    }

    return EXIT_SUCCESS;
}
