int main()
{
    std::mt19937 e;
    std::array ps = {1.0, 2.0, 3.0};
    std::discrete_distribution d(ps.begin(), ps.end());

    for( int i=0; i!=10; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    std::mt19937 e2;
    std::discrete_distribution d2({1.0, 2.0, 3.0});

    for( int i=0; i!=10; ++i){
        std::cout << d2(e2) << " "sv;
    }
    std::cout << std::endl;

    std::mt19937 e3;
    std::discrete_distribution d3 = {1.0, 2.0, 3.0};

    for( int i=0; i!=10; ++i){
        std::cout << d3(e3) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
