int main()
{
	// ラムダ式
	// [] ---- ラムダ式導入部
	// () ---- 引数
	// {} ---- 関数本体
	auto print = [](auto x)
	{
		std::cout << x << "\n"s;
	};

	print(123);
	print(3.14);
	print("Hello lambda!"s);

	auto print_two = [](auto x, auto y)
	{
		std::cout << x << " "s << y << "\n"s;
	};

	print_two(3, 4);
	print_two(5, "persons"s);
	print_two("Pi is"s, 3.14);

	auto no_argus = []()
	{
		std::cout << "No arguments!\n"s;
	};

	no_argus();

	// ランダム式そのもので関数呼び出し
	[](auto x){
		std::cout << x << "\n"s;
	}("Evaluate the lambda expression directly."s);

	auto plus = [](auto x, auto y)
		{ return x + y; };
	std::cout
		<< plus(1, 2) << "\n"s
		<< plus(1.5, 0.5) << "\n"s
		<< plus("add "s, "two strings"s) << "\n"s;

    // 戻り値型の指定
    auto onoff_to_bit = [](bool b) -> int
    {
        if (b)
            return 1.0;
        else
            return 0;
    };
    std::cout << onoff_to_bit(true) << std::endl;  // 1
    std::cout << onoff_to_bit(false) << std::endl; // 0

    // copy capture
    auto message = "hello"s;
    [=](){ std::cout << message << std::endl; }();

    // reference capture
    int x = 0;
    auto  f = [&](){ return ++x; };
    std::cout << f() << " "s << x << std::endl;  // 1
    std::cout << f() << " "s << x << std::endl;  // 2
    std::cout << f() << " "s << x << std::endl;  // 3

    return EXIT_SUCCESS;
}
