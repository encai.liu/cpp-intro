int main()
{
    // unlike vector, the size of array is determined when defined
    std::array<int, 10> a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << "a[0] = "s << a[0] << std::endl;
    std::cout << "a.at[1] = "s << a.at(1) << std::endl;

    // std::cout << "a[10] = "s << a[10] << std::endl;       // Indefinite value
    // std::cout << "a.at[10] = "s << a.at(10) << std::endl; // error, will throwing an instance of 'std::out_of_range'
 
    // copy an array
    std::array<int, 10> b = a;
    std::cout << "b[1] = "s << b[1] << std::endl;
    std::cout << "b.at[2] = "s << b.at(2) << std::endl;

    return EXIT_SUCCESS;
}
