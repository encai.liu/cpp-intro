template < typename Engine >
auto get_gamma_distribution( double alpha, double beta, Engine & e )
{
    std::gamma_distribution d( alpha, beta );
    return d(e);
}

int main()
{
    std::mt19937 e;

    // days untile next traffic accident
    for(auto i=0; i!=10; ++i)
    {
        std::cout << get_gamma_distribution(1.0, 1.0, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
