int main()
{
    std::allocator<std::string> a;

    // allocator_traits type
    using traits = std::allocator_traits<decltype(a)> ;

    // allocate
    std::string *p = traits::allocate(a, 1);

    // construct
    traits::construct(a, p, "hello");

    std::cout << *p << std::endl;

    // destory
    traits::destroy(a, p);

    // deallocate
    traits::deallocate(a, p, 1);

    return EXIT_SUCCESS;
}
