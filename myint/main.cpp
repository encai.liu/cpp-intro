struct Integer
{
    int *ptr;
public:
    explicit Integer( int value = 0 )
        : ptr ( new int(value) ) { }
    ~Integer()
    { delete ptr; }

    // copy
    Integer( const Integer & r )
        : ptr ( new int(*r.ptr) ) { }

    Integer & operator = ( const Integer & r)
    {
        if( this != &r )
            *ptr = *r.ptr;
        return *this;
    }

    // move
    Integer ( Integer && r)
        : ptr ( r.ptr )
    {
        r.ptr = nullptr;
    }

    Integer & operator = ( Integer && r )
    {
        delete ptr;
        ptr = r.ptr;
        r.ptr = nullptr;
        return *this;
    }

    // compound assignment operator
    Integer & operator += ( Integer const & r )
    {
        *ptr += *r.ptr;
        return *this;
    }

    Integer & operator += ( Integer && r )
    {
        *ptr += *r.ptr;
        r.ptr = nullptr;
        return *this;
    }

    Integer & operator -= ( Integer & r )
    {
        *ptr -= *r.ptr;
        return *this;
    }

    Integer & operator -= ( Integer && r )
    {
        *ptr -= *r.ptr;
        return *this;
    }

    // unary operator
    Integer operator -() const
    {
        Integer result( -*ptr );
        return result;
    }

    Integer operator +() const
    {
        Integer result( *ptr );
        return result;
    }

    // binary operator
//    Integer operator +( const Integer & r ) const
//    {
//        return Integer( *ptr + *r.ptr );
//    }
    friend Integer operator +( const Integer  & l, const Integer  & r );
    friend Integer operator +( const Integer && l, const Integer  & r );
    friend Integer operator +( const Integer  & l, const Integer && r );
    friend Integer operator +( const Integer && l, const Integer && r );
};

Integer operator +( const Integer & l, const Integer & r )
{
    return Integer( *l.ptr + *r.ptr );
}

Integer operator +( const Integer && l, const Integer & r )
{
    auto result = std::move(l);
    result += r;
    return result;
}

Integer operator +( const Integer & l, const Integer && r )
{
    auto result = std::move(r);
    result += l;
    return result;
}

Integer operator +( const Integer && l, const Integer && r )
{
    return std::move(l) + r;
}

int main()
{
    Integer a(1);
    Integer b;

    b = a;
    std::cout << "a/b = "s << *b.ptr << std::endl ;

    b += a;
    std::cout << "b = "s << *b.ptr << std::endl ;

    b += -a;
    std::cout << "b = "s << *b.ptr << std::endl ;

    Integer c = a + b;
    std::cout << "c = "s << *c.ptr << std::endl ;

    auto d = (a + a) + (b + b);
    std::cout << "d = "s << *d.ptr << std::endl ;

    return EXIT_SUCCESS;
}

