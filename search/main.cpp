int main()
{
    std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> v2 = {4, 5, 6}; 
    std::vector<int> v3 = {3, 5, 6}; 
    
    std::cout << std::boolalpha;
    auto a = std::search(v1.begin(), v1.end(), v2.begin(), v2.end()); 
    if (a != v1.end()){
        std::cout << "Found." << std::endl;     // oooooo
    }else{
        std::cout << "Not found." << std::endl;
    }

    auto b = std::search(v1.begin(), v1.end(), v3.begin(), v3.end()); 
    if (b != v1.end()){
        std::cout << "Found." << std::endl;
    }else{
        std::cout << "Not found." << std::endl; // oooooo 
    }

    return EXIT_SUCCESS;
}
