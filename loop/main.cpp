int input()
{
    std::cout << ">"s;
    int x{};
    std::cin >> x;
    return x;
}

int main()
{
    // goto
    std::cout << 1;

    goto skip;
    std::cout << 2;
skip:
    std::cout << 3 << "\n\n"s;

    // endless loop use goto
    int sum = 0;
loop:
    int x = input();
    if (x != 0){
        sum += x;
        std::cout << sum << "\n"s;
        goto loop;
    }
    std::cout << "\n\n"s;

    // while
    sum = 0;
    while( (x = input()) != 0) {
        sum += x;
        std::cout << sum << "\n"s;
    }
    std::cout << "\n\n"s;

    // for
    for(auto i = 1; i <= 9; i++, std::cout << "\n"s ){
        for(auto j = 1; j <= 9; j++){
            std::cout << i * j << "\t"s;
        }
        // std::cout << "\n"s;
    }
    std::cout << "\n"s;

    // do { } while ();
    auto i = 1;
    do{ 
        auto j = 1;
        do{
            std::cout << i * j << "\t"s;
            j++;
        }while( j <= 9 );

        std::cout << "\n"s;
        i++;
    }while( i <= 9 );
    std::cout << "\n"s;

    // while () { }
    i = 1;
    while( i <= 9 ) { 
        auto j = 1;
        while( j <= 9 ) {
            std::cout << i * j << "\t"s;
            j++;
        }
        std::cout << "\n"s;
        i++;
    }   
    std::cout << "\n"s;

    return EXIT_SUCCESS;
}
