template <typename Engine>
auto coinflips100(Engine &e)
{
    // t = 100, p = 0.5
    std::binomial_distribution d(100, 0.5);
    return d(e);
}

template <typename Engine>
auto roll_for_one(Engine &e)
{
    // t = 60, p = 1.0/6.0
    std::binomial_distribution d(60, 1.0/6.0);
    return d(e);
}

template <typename Engine>
auto lootbox(Engine &e)
{
    // t = 100, p = 0.01
    std::binomial_distribution d(100, 0.01);
    return d(e);
}

int main()
{
    std::mt19937 e;

    for(auto i=0; i!=10; ++i){
        std::cout << coinflips100(e) << " "sv;
    }
    std::cout << std::endl;

    for(auto i=0; i!=10; ++i){
        std::cout << roll_for_one(e) << " "sv;
    }
    std::cout << std::endl;

    for(auto i=0; i!=10; ++i){
        std::cout << lootbox(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
