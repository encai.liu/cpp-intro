int main()
{
    std::mt19937 e;
    std::student_t_distribution d(1.0);

    std::cout << "d.n(): "sv << d.n() << std::endl;

    for( int i=0; i!=10; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
