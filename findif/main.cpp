int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5};

    // find
    auto pos = std::find(v.begin(), v.end(), 3);

    if(pos == v.end()){
        std::cout << "Not found.\n"s;
    }else{
        std::cout << "Found.\n"s;
        std::cout << *pos << "\n"s;
    }

    pos = std::find(v.begin(), v.end(), 0);

    if(pos == v.end()){
        std::cout << "Not found.\n"s;
    }else{
        std::cout << "Found.\n"s;
        std::cout << *pos << "\n"s;
    }

    // find_if
    auto is_even = [](auto value){
        return (value%2 == 0);
    };
    auto is_odd = [](auto value){
        return (value%2 == 1);
    };

    // find first even value
    auto first_even = find_if(v.begin(), v.end(), is_even);
    auto first_odd = find_if(v.begin(), v.end(), is_odd);
    std::cout << "First even value: "s << *first_even << "\n"s;
    std::cout << "First odd value: "s<< *first_odd << "\n"s;

    return EXIT_SUCCESS;
}
