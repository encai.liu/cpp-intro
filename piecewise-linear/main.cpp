int main()
{
    std::mt19937 e;
    auto bs = {0.0, 1.0, 2.0};
    auto ps = {1.0, 2.0, 1.2};

    std::piecewise_linear_distribution d1(bs.begin(), bs.end(), ps.begin());

    for(auto i=0; i<10; ++i){
        std::cout << d1(e) << " "sv;
    }
    std::cout << "\n"sv;

    std::piecewise_linear_distribution d2(
        {1.0, 2.0, 3.0},
        [] (auto x) { return x; }    
    );

    for(auto i=0; i<10; ++i){
        std::cout << d2(e) << " "sv;
    }
    std::cout << "\n"sv;

    std::piecewise_linear_distribution d3(
        5.0, 
        1.0, 5.0,
        [] (auto x) { return x; }    
    );

    for(auto i=0; i<10; ++i){
        std::cout << d3(e) << " "sv;
    }
    std::cout << "\n"sv;

    return EXIT_SUCCESS;
}
