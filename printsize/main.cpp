template <typename T>
void print_size()
{
    std::cout<< sizeof(T) << std::endl;
}


int main()
{
    // integer types
    std::cout << "integer types:\n"s; 
    print_size<char>();         // 1
    print_size<short>();        // 2
    print_size<int>();          // 4
    print_size<long>();         // 8
    print_size<long long>();    // 8

    // float types
    std::cout << "float types:\n"s;
    print_size<float>();        // 4
    print_size<double>();       // 8
    print_size<long double>();  // 16

    // pointer
    std::cout << "pointer types:\n"s; 
    print_size<char *>();
    print_size<short *>();
    print_size<int *>();
    print_size<long *>();
    print_size<long long *>();
    print_size<float *>();
    print_size<double *>();
    print_size<long double *>();

    return EXIT_SUCCESS;
}
