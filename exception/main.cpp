int main()
{
//    std::array<int, 10> a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
//    std::cout << a.at(20) << std::endl;

    std::cout << "Don't input 0.>"s;

    int input;
    std::cin >> input;
    
    if(input == 0)
        throw 0;

    std::cout << "Success."s << std::endl;

    // try{} catch () {}
    try{
        //throw 123;       // int
        //throw 123.1;     // double
        throw "123.1s"s; // double
    }
    catch(int e){
        std::cout << "Catched int exception: "s << e << std::endl;
    }
    catch(double e){
        std::cout << "Catched double exception: "s << e << std::endl;
    }
    catch(std::string & e){
        std::cout << "Catched string exception: "s << e << std::endl;
    }

    return EXIT_SUCCESS;
}
