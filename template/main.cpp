template <typename T>
T twice(T const & x){
    return x * 2;
}

template <typename T>
T devided_by_2(T const & x){
    return x / 2;
}
 
template <typename T>
void print(T const & x){
    std::cout << x << std::endl;
}

int main()
{
    print( twice(1) );            // 2
    print( twice(2.1) );          // 4.2
    print( "Hello, template!"s );

    print( devided_by_2(5) );     // 2, same as devided_by_2<int>(2)
    print( devided_by_2<double>(5) ); // 2.5

    return EXIT_SUCCESS;
}
