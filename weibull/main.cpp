template < typename Engine >
auto get_weibull_distribution( double a, double b, Engine & e )
{
    std::weibull_distribution d( a, b);
    return d(e);
}

int main()
{
    std::mt19937 e;

    // days untile next traffic accident
    for(auto i=0; i!=10; ++i)
    {
        std::cout << get_weibull_distribution(1.0, 1.0, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
