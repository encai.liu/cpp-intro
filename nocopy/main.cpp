struct X
{
    // copy constructor
    X ( const X & ) = delete;
    // copy assignment
    X & operator = ( const X & ) = delete;

    X() { }

    X( X && ) { }
    X & operator = ( const X && ) { return *this; }
};

int main()
{
    X a;

    // X b = a;   // error: use of deleted function

    X c = std::move(a); // OK

    return EXIT_SUCCESS;
}
