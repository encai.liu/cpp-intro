template <typename T>
struct cin_iterator
{
// --- boilerplate code
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using reference = T &;
    using pointer = T *;

    using iterator_category = std::input_iterator_tag;
// --- boilerplate code

    // constructor 
    cin_iterator ( bool fail = false )
        : fail(fail)
    { ++*this; }

    reference operator *()
    { return value; }

    cin_iterator & operator ++()
    {
        if (!fail){
            std::cin >> value;
            fail = std::cin.fail();
        }
        return *this;
    }

    cin_iterator operator ++(int)
    {
        auto old = *this;
        ++*this;
        return old;
    }

    bool fail;          // status
    value_type value;   // cache
};

template <typename  Container>
void print(Container const & c)
{
    for(std::size_t i=0; i != c.size(); ++i){
        std::cout << c[i];
    }
    std::cout << std::endl;
}

// compare
template <typename T>
bool operator ==(cin_iterator<T> const & l, cin_iterator<T> const & r)
{ return l.fail == r.fail; }

template <typename T>
bool operator !=(cin_iterator<T> const & l, cin_iterator<T> const & r)
{ return !(l == r); }

int main()
{
    std::cout << "buffer1: \n"s;

    cin_iterator<int> input, fail(true);
    std::vector<int> buffer1{};
    std::copy( input, fail, std::back_inserter(buffer1) );
    print(buffer1);

    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::cout << "buffer2: \n"s;

    std::istream_iterator<int> inter(std::cin), end_iter;
    std::vector<int> buffer2{};
    std::copy( inter, end_iter, std::back_inserter(buffer2) );
    print(buffer2);

    return EXIT_SUCCESS;
}

