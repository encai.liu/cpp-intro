int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5};
    std::vector<int> dest(10);

    auto next = std::copy(v.begin(), v.end(), dest.begin());
    for (auto iter = dest.begin(); iter != dest.end(); ++iter){
        std::cout << *iter << " "s;
    }   // 1 2 3 4 5 0 0 0 0 0 
    std::cout << std::endl;

    std::copy(v.begin(), v.end(), next);
    for (auto iter = dest.begin(); iter != dest.end(); ++iter){
        std::cout << *iter << " "s;
    }   // 1 2 3 4 5 1 2 3 4 5 
    std::cout << std::endl;
    return EXIT_SUCCESS;
}
