int main()
{
    // pointer VS reference: substitute & initialize
    int x{ 1 };
    int y{ 2 };

    // initialize
    //int & e_ref;  // error: ‘e_ref’ declared as reference but not initialized
    int & ref = x;
    ref = y;      // x <- y
    std::cout << "x = "s << x << ", y = "s << y << ".\n";

    // substitute
    int *pointer; // It's ok for a pointer.
    pointer = &y;
    *pointer = 3; // y <- 3
    std::cout << "x = "s << x << ", y = "s << y << ".\n";

    // nullptr
    //double *p_dbl = nullptr;
    //std::string *p_str = nullptr;

    //std::cout << *p_dbl; // Segmentation fault
    //std::cout << *p_str; // Segmentation fault

    // pointer to const int
    const int data = 123;
    const int *ptr = &data;
    std::cout << *ptr << std::endl;
    //*ptr = 0;  // error: assignment of read-only location ‘* ptr’

    int data2 = 456;
    ptr = &data2; // but you can assign a new value to the pointer
    std::cout << *ptr << std::endl;

    // const pointer
    int * const ptr2 = &data2;
    *ptr2 = 789;
    std::cout << *ptr2 << std::endl;

    int data3 = 12;
    // ptr2 = &data3;  // error: assignment of read-only variable ‘ptr2’ 

    // both the pointer and the location are read-only
    const int * const ptr3 = &data3;
    // *ptr3 = 0;      // error: assignment of read-only location ‘*(const int*)ptr3’
    // ptr3 = &data;   // error: assignment of read-only variable ‘ptr3’
    std::cout << *ptr3 << std::endl;

    return EXIT_SUCCESS;
}
