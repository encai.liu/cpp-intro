int main()
{
    std::vector<int> a = {1, 2, 3, 4, 5};
    std::vector<int> b = {1, 2, 3, 4, 5};
    std::vector<int> c = {1, 2, 3, 4, 5, 6};
    std::vector<int> d = {1, 2, 2, 4, 6};

    // std::equal
    std::cout << std::boolalpha;
    std::cout << std::equal(a.begin(), a.end(),             // true
                            b.begin(), b.end()) << std::endl;
    std::cout << std::equal(a.begin(), a.end(),             // false
                            c.begin(), c.end()) << std::endl;
    std::cout << std::equal(a.begin(), a.end(),             // false 
                            d.begin(), d.end()) << std::endl;
    
    // size
    auto first = a.begin();
    auto last = a.end();
    auto size = last - first;
    std::cout << size << std::endl;     // 5
    size = std::distance(first, last);
    std::cout << size << std::endl;     // 5

    std::vector<double> v = {1.3, 2.2, 3.0, 4.9, 5.7};
    std::vector<double> w = {1.9, 2.4, 3.8, 4.5, 5.0};
    // compare function
    auto comp = [](auto a, auto b)
    {
        return (std::floor(a) == std::floor(b));
    };
    bool is_floor_equal = std::equal(v.begin(), v.end(),
                                     w.begin(), w.end(),
                                     comp);
    std::cout << is_floor_equal << std::endl;   // true

    return EXIT_SUCCESS;
}
