
int f(int x)
{
    std::cout << x << std::endl;
    return x;
}

using f_type = int( int );
using f_pointer = f_type *;

f_pointer g(f_pointer f){
    f(0);
    return f;
}

int main()
{

    // f_pointer f_ptr = &f;
    auto (*f_ptr)(int) -> int = &f; 

    (*f_ptr)(123); // you can call the function through the pointer.
    f_ptr(456);    // you can use the function pointer like tihs.

    g(f_ptr)(789);

    return EXIT_SUCCESS;
}
