template <typename T>
class dynamic_array
{
private:
    T *first;
    T *last;
public:
    dynamic_array( std::size_t size = 0 )
        : first( new T[size] ), last( first + size )
    { }

    // copy constructor
    dynamic_array( const dynamic_array & r )
        : first( new T[r.size()] ), last( first + r.size() )
    {
        std::copy( r.begin(), r.end(), begin() );
    }
    
    // move constructor
    dynamic_array( dynamic_array && r)
        : first( r.first ), last( r.last )
    {
        r.first = nullptr;
        r.last = nullptr;
    }

    ~dynamic_array()
    { delete [] first; }

    T & operator [] (std::size_t i ) const noexcept
    { return first[i]; }

    std::size_t size() const noexcept
    { return last - first; }

    T * begin() const noexcept
    { return first; }

    T * end() const noexcept
    { return last; }

    // assignment copy operator
    dynamic_array & operator = ( const dynamic_array & r )
    {
        if ( (this != &r) && (size() != r.size()) ){
            delete first;

            first = new T[r.size()];
            last = first + r.size();

            std::copy( r.begin(), r.end(), begin() );
        }
        return *this;
    }

    // assignment move operator
    // ATTENTION: Source and destination must not be same!!!
    dynamic_array & operator = ( const dynamic_array && r)
    {
        delete first;

        first = r.first;
        last = r.last;

        r.first = nullptr;
        r.last = nullptr;

        return *this;
    }
};

int main()
{
    dynamic_array<int> a(5);

    for(auto i=0; i!=5; ++i){
        a[i] = i;
    }

    std::for_each( std::begin(a), std::end(a),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    dynamic_array<int> b(a);

    std::for_each( std::begin(b), std::end(b),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    dynamic_array<int> c(10);
    std::cout << "before assignment copy, c.size(): "s << c.size() << std::endl;
    c = a;
    std::cout << "after assignment copy, c.size(): "s << c.size() << std::endl;

    std::for_each( std::begin(c), std::end(c),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    return EXIT_SUCCESS;
}
