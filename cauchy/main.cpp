int main()
{
    std::mt19937 e;
    std::cauchy_distribution d(0.0, 1.0);

    std::cout << "d.a(): "sv << d.a() << std::endl;
    std::cout << "d.b(): "sv << d.b() << std::endl;

    for( int i=0; i!=10; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
