int main()
{
	auto answer = 42;
	std::cout << answer << "\n"s;

	// auto pi = 3.14;
	auto pi(3.14);	//<- 変数の初期化は()を使っても良い
	std::cout << pi << "\n"s;

	// auto question = "Life, The Universe, and Everything."s;
	auto question{"Life, The Universe, and Everything."s};	//<- {}を使っても良い
	std::cout << question << "\n"s;
}
