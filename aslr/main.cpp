// check if the os is ASLR(Address Space Layout Random)

template<typename To, typename From>
inline constexpr To bit_cast(const From& from) noexcept {
  typename std::aligned_storage<sizeof(To), alignof(To)>::type storage;
  std::memcpy(&storage, &from, sizeof(To));
  return reinterpret_cast<To&>(storage);
  // More common implementation:
  // std::remove_const_t<To> to{};
  // std::memcpy(&to, &from, sizeof(To));
  // return to;
}

int main()
{
    int data{};

    // In an ASLR OS, you may get different reult each time running the program;
    std::cout << bit_cast<std::uintptr_t>(&data) << std::endl;

    return EXIT_SUCCESS;
}
