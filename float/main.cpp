int main()
{
    float a = 0.0;
    float b = -0.0;

    // 0.0 and -0.0
    std::cout << a << "\n"s;
    std::cout << b << "\n"s;

    bool c = (a==b);
    std::cout << std::boolalpha;
    std::cout << "(0.0 == -0.0): "s << c << "\n"s;

	// data size
	std::cout << "Size of float: "s << sizeof(float) << "\n"s;
	std::cout << "Size of double: "s << sizeof(double) << "\n"s;
	std::cout << "Size of long double: "s << sizeof(long double) << "\n"s;

	// precision
	a = 10000.0f;
	b = 0.0001f;

	std::cout << "a = "s << a << "\n"s;
	std::cout << "b = "s << b << "\n"s;
	std::cout << "a + b = "s << a + b << "\n"s;

	double aa = 10000.0;
	double bb = 0.0001;

	std::cout << "aa = "s << aa << "\n"s;
	std::cout << "bb = "s << bb << "\n"s;
	std::cout << "aa + bb = "s << aa + bb << "\n"s;

	long double aaa = 10000.0L;
	long double bbb = 0.0001L;

	std::cout << "aaa = "s << aaa << "\n"s;
	std::cout << "bbb = "s << bbb << "\n"s;
	std::cout << "aaa + bbb = "s << aaa + bbb << "\n"s;

	// infinity
	aa =  std::numeric_limits<double>::infinity();
	bb = -std::numeric_limits<double>::infinity();

	std::cout << "Infinity: "s << aa << "\n"s;
	std::cout << "Minus infinity: "s << bb << "\n"s;

	// NaN: Not a nubmer
	double NaN = std::numeric_limits<double>::quiet_NaN();

	std::cout << "NaN: "s << NaN << "\n"s;

	std::cout << std::boolalpha;
	std::cout << "  NaN != 0.0: "s << (NaN != 0.0) << "\n"s;
	std::cout << "  NaN == 0.0: "s << (NaN == 0.0) << "\n"s;
	std::cout << "  NaN == NaN: "s << (NaN == NaN) << "\n"s;
	std::cout << "  NaN != NaN: "s << (NaN != NaN) << "\n"s;
	std::cout << "  NaN < 0.0: "s << (NaN < 0) << "\n"s;
	std::cout << "  NaN > 0.0: "s << (NaN > 0) << "\n"s;

    // Number of significant digits
    std::cout << "Number of significant digits:\n"s;
    std::cout << "  float: "s << std::numeric_limits<float>::digits10 << "\n"s;
    std::cout << "  double: "s << std::numeric_limits<double>::digits10 << "\n"s;
    std::cout << "  long double: "s << std::numeric_limits<long double>::digits10 << "\n"s;

    // epsilon
    std::cout << "Epsilon:\n"s;
    std::cout << "  float: "s << std::numeric_limits<float>::epsilon() << "\n"s;
    std::cout << "  double: "s << std::numeric_limits<double>::epsilon() << "\n"s;
    std::cout << "  long double: "s << std::numeric_limits<long double>::epsilon() << "\n"s;

    // float data type conversion
    auto da = 1.0f + 1.0f;
    auto db = 1.0f + 1.0;
    auto dc = 1.0f + 1.0l;

    const std::type_info& infoa = typeid(da);
    const std::type_info& infob = typeid(db);
    const std::type_info& infoc = typeid(dc);

    std::cout << "Data conversion:\n"s;
    std::cout << "float + float -> "s << infoa.name() << "\n"s;
    std::cout << "float + double -> "s << infob.name() << "\n"s;
    std::cout << "float + long double -> "s << infoc.name() << "\n"s;
    
    return EXIT_SUCCESS;
}
