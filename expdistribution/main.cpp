template < typename Engine >
auto until_next_traffic_accident( double lambda, Engine & e )
{
    std::exponential_distribution d( lambda );  // n accidents / month
    return d(e) * 30.0;                         // 30 days / month
}

int main()
{
    std::mt19937 e;

    // days untile next traffic accident
    for(auto i=0; i!=10; ++i)
    {
        std::cout << until_next_traffic_accident(10.0, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
