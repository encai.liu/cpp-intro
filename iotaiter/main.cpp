template <typename T>
struct iota_iterator
{
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using reference = T &;
    using pointer = T *;
    using iterator_category = std::forward_iterator_tag;

    // the value
    T value;

    // constructor
    iota_iterator( T value = 0 )
        : value (value)
    { }

    reference operator *() noexcept
    { return value; }

    const reference operator *() const noexcept
    { return value; }

    // operator ++ (pre-)
    iota_iterator & operator ++() noexcept
    {
        ++value;
        return *this;
    }

    // operator ++ (post-)
    iota_iterator operator ++(int)
    {
        auto temp = *this;
        ++*this;
        return temp;
    }

    // operator ==
    bool operator ==(iota_iterator & i) const noexcept
    {
        return (value == i.value);
    }

    // operator !=
    bool operator !=(iota_iterator & i) const noexcept
    {
        return !(*this == i);
    }
};

int main()
{
    iota_iterator first(0), last(10);

    std::for_each(first, last, 
        [](auto i){ std::cout << i; });
    std::cout << std::endl;

    std::vector<int> v;
    std::copy(first, last, std::back_inserter(v));

    std::for_each(v.begin(), v.end(), 
        [](auto i){ std::cout << i; });
    std::cout << std::endl;

    iota_iterator iter(0);
    std::cout << *iter++ << std::endl;  // 0
    std::cout << *iter << std::endl;    // 1
    std::cout << *++iter << std::endl;  // 2

    return EXIT_SUCCESS;
}
