struct cout_iterator
{
    // ----boilerplate code start
    // we only use this iterator as an output.
    using difference_type = void;
    using value_type = void;
    using reference = void;
    using pointer = void;

    using iterator_category = std::output_iterator_tag;

    // do nothing
    cout_iterator & operator *() { return *this; }
    cout_iterator & operator ++() { return *this; }
    cout_iterator & operator ++(int) { return *this; }
    // ----boilerplate code end

    template <typename T>
    cout_iterator & operator = (T const & x)
    {
        std::cout << x;
        return *this;
    }
};

template <typename Container>
struct back_inserter 
{
    back_inserter ( Container & c )
        : c(c) { }

    // other boilerplate code

    back_inserter & operator = ( const typename Container::value_type & value ){
        c.push_back(value);
    }

    Container & c;
};

template <typename  Container>
void print(Container const & c)
{
    for(std::size_t i=0; i != c.size(); ++i){
        std::cout << c[i];
    }
    std::cout << std::endl;
}

int main()
{
    std::array<int, 5> a = {1, 2, 3, 4, 5};
    std::vector<int> v(5);

    // use original print function
    std::copy(std::begin(a), std::end(a), std::begin(v));
    print(v);

    // use original cout_iterator
    cout_iterator out;
    std::copy(std::begin(a), std::end(a), out);
    std::cout << std::endl;

    // use std::ostream_iterator<T>
    std::ostream_iterator<int> out2(std::cout);
    std::copy(std::begin(a), std::end(a), out2);
    std::cout << std::endl;

    std::vector<int> aa = {1, 2, 3, 4, 5};
    std::vector<int> temp;
    //auto out3 = back_inserter(temp); // error: no type named ‘value_type’ in ‘struct std::iterator_traits<back_inserter<std::vector<int> > >’
    auto out3 = std::back_inserter(temp); 
    std::copy(std::begin(aa), std::end(aa), out3);
    print(temp);

    return EXIT_SUCCESS;
}
