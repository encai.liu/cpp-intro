int main()
{
    // ./main > cout.txt 2>cerr.txt

    // standard out
    std::cout << "This will be print to standard out.\n"s;          // -> cout.txt

    // standard error out
    std::cerr << "This will be print to standard error out.\n"s;    // -> cerr.txt

    return EXIT_SUCCESS;
}
