struct Object
{
    int data_member;
    void member_func(){
       std::cout << data_member << std::endl;
    }
};

int main()
{
    // pointer to Object's data_member
    int Object::* int_ptr = &Object::data_member;
    // pointer to Object's member_func
    void (Object::* func_ptr)() = &Object::member_func;

    Object object;

    object.*int_ptr = 123;
    (object.*func_ptr)();           // print 123

    Object another_obj;

    another_obj.data_member = 456;
    (another_obj.*func_ptr)();      // print 456

    auto c_ptr = &object;
    c_ptr->*int_ptr = 789;
    (c_ptr->*func_ptr)();           // print 789
    
    (*c_ptr).*int_ptr = 234;
    ((*c_ptr).*func_ptr)();         // print 234

    std::invoke(int_ptr, c_ptr) = 345;
    std::invoke(func_ptr, c_ptr);   // print 345

    return EXIT_SUCCESS;
}
