int main()
{
    std::mt19937 e;
    std::lognormal_distribution d(0.0, 1.0);

    std::cout << "d.m(): "sv << d.m() << std::endl;
    std::cout << "d.s(): "sv << d.s() << std::endl;

    for( int i=0; i!=20; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
