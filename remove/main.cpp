int main()
{
    std::vector<int> a = {1, 2, 3, 3, 3, 4, 5, 4, 3};
    auto print_all = [](auto first, auto last){
        for(auto iter = first; iter != last; ++iter){
            std::cout << *iter << " "s;
        }
        std::cout << std::endl;
    };
    auto is_odd = [](auto value){ return (value%2 == 1); };

    // remove
    auto new_last = std::remove(a.begin(), a.end(), 3);
    print_all(a.begin(), new_last);

    // remove_if
    auto new_last2 = std::remove_if(a.begin(), new_last, is_odd);
    print_all(a.begin(), new_last2);

    return EXIT_SUCCESS;
}
