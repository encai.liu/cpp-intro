int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5};

    std::reverse_iterator first1{ std::end(v) };
    std::reverse_iterator last1{ std::begin(v) };

    std::for_each( first1, last1, 
        [](auto x){ std::cout << x ; });
    std::cout << std::endl;

    auto first2 = std::rbegin(v);
    auto last2 =  std::rend(v);

    std::for_each( first2, last2, 
        [](auto x){ std::cout << x ; });
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
