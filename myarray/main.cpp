struct array_int_10{
    int idata[10];

    int & operator [](std::size_t i){
        return idata[i];
    }
};

template <typename Array>
struct array_iterator{
    Array & a;
    std::size_t i;

    array_iterator(Array & a, std::size_t i) 
    : a(a), i(i) {}

    // return current element
    typename Array::reference operator *()
    {  return a[i];  }

    array_iterator & operator ++()
    {
        ++i;
        return *this;
    }

    array_iterator operator ++(int)
    {
        array_iterator copy = *this;
        ++*this;
        return copy;
    }

    array_iterator & operator --()
    {
        --i;
        return *this;
    }

    array_iterator operator --(int)
    {
        array_iterator copy = *this;
        --*this;
        return copy;
    }

    bool operator ==(array_iterator & right)
    {
        return (i == right.i);
    }

    bool operator !=(array_iterator & right)
    {
        return (i != right.i);
    }

    array_iterator & operator +=(std::size_t n)
    {
        i += n;
        return *this;
    }

    array_iterator operator +(std::size_t n) const
    {
        auto copy = *this;
        copy += n;
        return copy;
    }

    typename Array::reference
    operator [](std::size_t n) const
    {
        return *(*this + n);
    }

    bool operator > (array_iterator const & right) const
    {
        return (i > right.i);
    }

    bool operator < (array_iterator const & right) const
    {
        return (i < right.i);
    }

    bool operator >= (array_iterator const & right) const
    {
        return (i >= right.i);
    }

    bool operator <= (array_iterator const & right) const
    {
        return (i <= right.i);
    }
};

template <typename Array>
struct array_const_iterator{
    Array const & a;
    std::size_t i;

    // constractor
    array_const_iterator(Array const & a, std::size_t i)
        : a(a), i(i){ }

    array_const_iterator(typename array_iterator<Array>::iterator const & iter)
        : a(iter.a), i(iter.i){ }

    array_const_iterator & operator ++()
    {
        ++i;
        return *this;
    }

    array_const_iterator operator ++(int)
    {
        array_const_iterator copy = *this;
        ++*this;
        return copy;
    }

    array_const_iterator & operator --()
    {
        --i;
        return *this;
    }

    array_const_iterator operator --(int)
    {
        array_const_iterator copy = *this;
        --*this;
        return copy;
    }

    bool operator ==(array_const_iterator & right)
    {
        return (i == right.i);
    }

    bool operator !=(array_const_iterator & right)
    {
        return (i != right.i);
    }

    array_const_iterator & operator +=(std::size_t n)
    {
        i += n;
        return *this;
    }

    array_const_iterator operator +(std::size_t n) const
    {
        auto copy = *this;
        copy += n;
        return copy;
    }

    typename Array::reference
    operator [](std::size_t n) const
    {
        return *(*this + n);
    }

    bool operator > (array_const_iterator const & right) const
    {
        return (i > right.i);
    }

    bool operator < (array_const_iterator const & right) const
    {
        return (i < right.i);
    }

    bool operator >= (array_const_iterator const & right) const
    {
        return (i >= right.i);
    }

    bool operator <= (array_const_iterator const & right) const
    {
        return (i <= right.i);
    }
};

template <typename T, std::size_t N >
struct array{

    using value_type = T;
    using reference = T &;
    using const_reference = const T &;
    using size_type = std::size_t;
    using iterator = array_iterator<array>;
    using const_iterator = array_const_iterator<array>;

    const_iterator cbegin() const
    {
        return const_iterator(*this, 0);
    }

    const_iterator begin() const
    {
        return const_iterator(*this, 0);
    }

    iterator begin(){
        return array_iterator(*this, 0);
    }

    const_iterator cend() const
    {
        return const_iterator(*this, N);
    }

    const_iterator end() const
    {
        return const_iterator(*this, N);
    }

    iterator end(){
        return array_iterator(*this, N);
    }

    value_type data[N];

    reference & operator [](size_type i)
    {
        if( i>N )
            throw std::out_of_range("Error: Our of range");

        return data[i];
    }

    reference at(size_type i)
    {
        if( i>N )
            throw std::out_of_range("Error: Our of range");

        return data[i];
    }

    const_reference & operator [](size_type i) const
    {
        if( i>N )
            throw std::out_of_range("Error: Our of range");

        return data[i];
    }

    size_type size()
    {
        return N;
    }

    size_type size() const
    {
        return N;
    }

    reference front()
    {
        return data[0];
    }

    const_reference front() const
    {
        return data[0];
    }

    reference back()
    {
        return data[N-1];
    }

    const_reference back() const
    {
        return data[N-1];
    }

    void fill(const value_type u){
        for(std::size_t i = 0; i != N; ++i){
            data[i] = u;
        }
    }
};

template <typename  Container>
void print(Container const & c)
{
    for(std::size_t i=0; i != c.size(); ++i){
        std::cout << c[i] << ", "s;
    }
    std::cout << std::endl;
}


int main()
{
    using array_type = array<int, 10>;

    array_int_10 a = {0, 1, 2};
    array<int, 10> b = {10, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    array_type c = {0, 1, 2};

    std::cout << "a[1] = "s << a[1] << std::endl;
    std::cout << "b[1] = "s << b[1] << std::endl;
    std::cout << "c[1] = "s << c[1] << std::endl;

    // using nested type outside the class
    array_type::value_type x = 0;
    array_type::reference r1 = c[2];
    std::cout << "x = "s << x << std::endl;       // 0
    std::cout << "r1 = "s << r1 << std::endl;     // 2
    r1 = 4;
    std::cout << "c[2] = "s << c[2] << std::endl; // 4

    auto r2 = c[2];                               // Not a reference, same as int r2 = c[2];
    std::cout << "r2 = "s << r2 << std::endl;     // 4
    r2 = 5;
    std::cout << "c[2] = "s << c[2] << std::endl; // 4
    std::cout << "r2 = "s << r2 << std::endl;     // 5

    int r3 = c[2];
    std::cout << "r3 = "s << r3 << std::endl;     // 4
    r3 = 6;
    std::cout << "c[2] = "s << c[2] << std::endl; // 4
    std::cout << "r3 = "s << r3 << std::endl;     // 6

    // size
    std::cout << "c.size() = "s << c.size() << std::endl;

    // print(a); // error
    print(b);
    print(c);

    // front() & back()
    std::cout << b.front() << std::endl;
    std::cout << b.back() << std::endl;

    // fill
    c.fill(11);
    print(c);

    // begin & ++ predix
    auto iter1 = b.begin();
    std::cout << std::endl;
    std::cout << "The first element of b is "s << *iter1 << ".\n";
    ++iter1;
    std::cout << "The second element of b is "s << *iter1 << ".\n";

    // end & -- prefix
    auto iter2 = b.end();
    --iter2;
    std::cout << "The last element of b is "s << *iter2 << ".\n";

    // begin & ++ suffix
    auto iter1b = b.begin();
    std::cout << std::endl;
    std::cout << "The first element of b is "s << *iter1b++ << ".\n";
    std::cout << "The second element of b is "s << *iter1b << ".\n";

    // end & -- suffix
    auto iter2b = b.end();
    iter2b--;
    std::cout << "The last element of b is "s << *iter2b << ".\n";

    // == & !=
    std::cout << std::boolalpha;
    std::cout << (iter1 == iter1b) << std::endl;
    std::cout << (iter1 == iter2b) << std::endl;

    std::for_each(std::begin(b), std::end(b), [](auto x){std::cout << x << std::endl;} );

    auto i = b.begin();
    i += 3;
    std::cout << "c[3] = "s << *i << std::endl;     // 3
    std::cout << "c[5] = "s << *(i+2) << std::endl; // 5

    auto j = b.begin();
    std::cout << "c[3] = "s << j[3] << std::endl;   // 3
    std::cout << "c[5] = "s << j[5] << std::endl;   // 5

    std::cout << (i > j) << std::endl;
    std::cout << (j > i) << std::endl;
    
    //std::cout << b[100] << std::endl;

    // at()
    std::cout << b.at(5) << std::endl;
    std::cout << b.at(50) << std::endl;

    return EXIT_SUCCESS;
}

