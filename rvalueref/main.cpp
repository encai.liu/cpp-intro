void f( const int & )
{
    std::cout << "lvalue\n"s;
}

void f( const int && )
{
    std::cout << "rvalue\n"s;
}

std::vector<int> g()
{
    std::vector<int> v;

    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    return v;  // maybe moved
}

int main()
{
    int object { };

    std::cout << "object: "s << object << "\n"s;

    f( object );            // lvalue
    f( object + object );   // rvalue, because no name?
    f( std::move(object) ); // rvalue
    f( []( int object ){ return object; }(object) ); // rvalue

    auto v = g();
    std::for_each( std::begin(v), std::end(v),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    return EXIT_SUCCESS;
}
