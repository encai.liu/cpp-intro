void use_old_style_ptr( int *p)
{
    std::cout << "Use old style ptr: *p = " << *p << "\n"s;
}

int main()
{
    // unique_ptr (https://cpprefjp.github.io/reference/memory/unique_ptr.html)
    auto p1 = std::make_unique< int > (0);
    std::cout << *p1 << "\n"s;

    *p1 = 123;
    std::cout << *p1 << "\n"s;

    // use get() to get the old style pointer
    use_old_style_ptr( p1.get() );

    auto p2 = std::make_unique< std::vector<int> >();
    p2->push_back(0);
    p2->push_back(1);
    p2->push_back(2);

    std::cout << "p2->size(): " << p2->size() << std::endl; 
    std::for_each( std::begin(*p2), std::end(*p2),
        []( auto x ){ std::cout << x << ", "; } );  // 0, 1, 2, 
    std::cout << std::endl; 

    auto p3 = std::make_shared<int>(0);
    auto p3_2 = p3;
    *p3 = 456;
    std::cout << "*p3 = "s << *p3 << "\n"s;     // 456
    std::cout << "*p3_2 = "s << *p3_2 << "\n"s; // 456

    // p1 will be disposed when going out of scope.
    return EXIT_SUCCESS;
}
