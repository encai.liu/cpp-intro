int main()
{
    // auto x = 1 + 1
    auto x = 1 + 1;
    auto y = x + 2;
    std::cout << y << "\n"s;

    std::cout << "EXIT_SUCCESS: "s << EXIT_SUCCESS << "\n"s;
    std::cout << "EXIT_FAILURE: "s << EXIT_FAILURE << "\n"s;

    // auto z = 1.0 % 1.0; // error
    // "hello"s << 1;      // May generate many meaningless errors

    return EXIT_SUCCESS;
}

