int input(){
    int x{};
    std::cout << ">"s;
    std::cin >> x;
    return x;
}

void loop_until_zero()
{
    if(input() == 0){
        return;
    } else {
        loop_until_zero();
    }
}

int factorial(int n)
{
    if (n == 0){
        return 0;
    }else if (n == 1){
        return 1;
    }else{
        return n * factorial(n-1);
    }
}

int main()
{
    loop_until_zero();

    std::cout << factorial(3) << "\n"s;
    std::cout << factorial(5) << "\n"s;

    return EXIT_SUCCESS;
}

