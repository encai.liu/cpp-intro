int main()
{
    std::vector<int> v = {1, 2, 1, 1, 3, 4, 5, 5, 6};

    // count
    auto a = std::count(v.begin(), v.end(), 1);
    auto b = std::count(v.begin(), v.end(), 2);
    auto c = std::count(v.begin(), v.end(), 5);
    // 3, 1, 2
    std::cout << a << ", "s << b << ", "s << c << std::endl;

    // count_if
    auto less_than_4 = [](auto value){ return (value < 4); };
    auto greater_than_4 = [](auto value){ return (value > 4); };  
    auto d = std::count_if(v.begin(), v.end(), less_than_4);
    auto e = std::count_if(v.begin(), v.end(), greater_than_4);
    // 5, 3
    std::cout << d << ", " << e << std::endl;

    return EXIT_SUCCESS;
}
