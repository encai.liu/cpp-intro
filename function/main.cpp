int plus(int x, int y)
{
	return x + y;
}

double plus(double x, double y)
{
	return x + y;
}

std::string plus(std::string x, std::string y)
{
	return x + y;
}

int main()
{
	auto x = plus(1, 2);
	std::cout << x << "\n"s;

	auto y = plus(1.05, 2.0);
	std::cout << y << "\n"s;

	auto s = plus("Hello, ", "function");
	std::cout << s << "\n"s;
}
