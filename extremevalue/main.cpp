template < typename Engine >
auto get_extreme_value_distribution( double a, double b, Engine & e )
{
    std::extreme_value_distribution d( a, b);
    return d(e);
}

int main()
{
    std::mt19937 e;

    // days untile next traffic accident
    for(auto i=0; i!=10; ++i)
    {
        std::cout << get_extreme_value_distribution(1.0, 1.0, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
