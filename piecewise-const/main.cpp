int main()
{
    std::mt19937 e;
    std::array bs = {-1.0, 1.0, 2.0};
    std::array ps = {5.0, 1.0};
    std::piecewise_constant_distribution d(bs.begin(), bs.end(), ps.begin());

    std::cout << "d: "sv << std::endl;
    for( int i=0; i!=10; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    std::cout << "\nd2: "sv << std::endl;
    std::piecewise_constant_distribution d2(
            {1.0, 2.0, 3.0, 4.0, 5.0},
            [](auto x) { return x; }
        );

    std::cout << "d2.intervals: "sv << std::endl;
    auto intervals = d2.intervals();
    for(auto iter=intervals.begin(); iter!=intervals.end(); ++iter){
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    std::cout << "d2.densities: " << std::endl;
    auto densities = d2.densities();
    for(auto iter=densities.begin(); iter!=densities.end(); ++iter){
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    for( int i=0; i!=10; ++i){
        std::cout << d2(e) << " "sv;
    }
    std::cout << std::endl;

    std::cout << "\nd3: "sv << std::endl;
    std::piecewise_constant_distribution d3(
            5, 1.0, 5.0,
            [](auto x) { return x; }
        );

    std::cout << "d3.intervals: "sv << std::endl;
    intervals = d3.intervals();
    for(auto iter=intervals.begin(); iter!=intervals.end(); ++iter){
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    std::cout << "d3.densities: " << std::endl;
    densities = d3.densities();
    for(auto iter=densities.begin(); iter!=densities.end(); ++iter){
        std::cout << *iter << " ";
    }
    std::cout << std::endl;

    for( int i=0; i!=10; ++i){
        std::cout << d3(e) << " "sv;
    }
    std::cout << std::endl;

   return EXIT_SUCCESS;
}
