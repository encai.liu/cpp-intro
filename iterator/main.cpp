namespace std{

    template <typename C>
    auto begin(C &c){
        return c.begin();
    }

    template <typename C>
    auto begin(C const &c){
        return c.begin();
    }

    template <typename C>
    auto end(C &c){
        return c.end();
    }

    template <typename C>
    auto end(C const &c){
        return c.end();
    }

}


int main()
{
    std::array<int, 5> a = {1, 2, 3, 4, 5};

    auto iter1 = a.begin();
    std::cout << "*(a.begin()) = "s << *iter1 << std::endl;    
    ++iter1;
    std::cout << "*(a.begin()+1) = "s << *iter1 << std::endl;

    auto iter2 = a.end();
    --iter2;
    std::cout << "*(a.end()-1) = "s << *iter2 << std::endl;
    --iter2;
    std::cout << "*(a.end()-2) = "s << *iter2 << std::endl;

    return EXIT_SUCCESS;
}
