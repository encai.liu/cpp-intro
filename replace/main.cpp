int main()
{
    std::vector<int> a = {1, 2, 3, 3, 3, 4, 5, 4, 3};
    auto print_all = [](auto first, auto last){
        for(auto iter = first; iter != last; ++iter){
            std::cout << *iter << " "s;
        }
        std::cout << std::endl;
    };

    // replace
    std::replace(std::begin(a), std::end(a), 3, 0);
    print_all(a.begin(), a.end());

    // fill
    std::fill(std::begin(a), std::end(a), 3);
    print_all(a.begin(), a.end());

    // fill_n
    std::fill_n(std::begin(a), 3, 4);
    print_all(a.begin(), a.end());

    // generate
    auto gen_ones = [](){ return 1; };
    std::generate(a.begin(), a.end(), gen_ones);
    print_all(a.begin(), a.end()); 

    // generate
    auto gen_zeros = [](){ return 0; };
    std::generate_n(a.end()-3, 3, gen_zeros);
    print_all(a.begin(), a.end()); 

    return EXIT_SUCCESS;
}
