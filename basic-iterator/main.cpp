int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5};

    auto iter = std::begin(v);
    const std::type_info& info_i = typeid(iter);
    std::cout << info_i.name() << "\n"s;

    // print all elements - 1
    for(std::size_t i = 0; i != std::size(v); ++i, ++iter){
         std::cout << *iter << "\n"s;
    }
    std::cout << "\n"s;

    // print all elements - 2
    for(std::vector<int>::iterator i = v.begin(); i != v.end(); ++i){
         std::cout << *i << "\n"s;
    }
    std::cout << "\n"s;

    // print all elements use lambda
    auto print_all = [](auto first, auto last){
        for(auto iter = first; iter != last; ++iter){
            std::cout << *iter << "\n"s;
        }
        std::cout << "\n"s;
    };

    print_all(v.begin(), v.end());

    // list all the files in current directory
    {
        std::filesystem::directory_iterator first("./"), last;
        print_all(first, last);
    }

    // list all the files in current directory
    {
        // input a non-number to quit
        std::istream_iterator<int> first(std::cin), last;
        print_all(first, last);
    }

    // put all elements to the destination using iterator
    auto output_all = [](auto first, auto last, auto out_iter){
        for(auto iter = first; iter != last; ++iter, ++out_iter){
           *out_iter = *iter;
        }
        std::cout << "\n"s;
    };
    output_all(v.begin(), v.end(), std::ostream_iterator<int>(std::cout));

    std::vector<int> dest(5);
    output_all(v.begin(), v.end(), dest.begin());
    print_all(v.begin(), v.end());

    return EXIT_SUCCESS;
}
