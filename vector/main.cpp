int input()
{
    int i{};

    std::cout << ">"s;
    std::cin >> i;
    return i;
}

int main()
{
    std::vector<int> v{};

    auto print_intv = [ ]( std::vector<int> v )
    {
        for(std::size_t j = 0; j != v.size(); ++j){
            std::cout << v.at(j) << " "s;
        }
        std::cout << "\n"s;
    };

    // input datas
    auto i = input();
    while(i != 0){
        v.push_back(i);
        i = input();        
    }
    print_intv(v);

    // sort
    auto size = v.size(); 
    for(std::size_t head = 0; head != size; ++head){
        auto min = head;
        for(std::size_t index = head+1; index != size; ++index){
            if(v.at(index) < v.at(min)){
                min = index;
            }
        }
        auto temp = v.at(head);
        v.at(head) = v.at(min);
        v.at(min) = temp;
    }
    print_intv(v);

    return EXIT_SUCCESS;
}
