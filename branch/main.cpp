int main()
{
    // 1. compound statement
    { // start ->
        std::cout << "Hello "s;
        std::cout << "compound statement.\n"s;
    } // <-end, ; is not nessesary.
    
    {
        auto a = 0;
        {
            auto b = 0;
            auto a = 1;
            std::cout << "b = "s << b << "\n"s;
            std::cout << "a = "s << a << "(inner compound statement)\n"s;
            // You can use b until here. 
        }
        // And you cannot use b here
        std::cout << "a = "s << a << "(outer compound statement)\n"s;
        // You can use a until here. 
    }

    // 2. conditional branch
    {
        auto a = 12345 + 6789;
        auto b = 8073 * 132 / 5;

        if (a < b) {
            std::cout << "b: "s << b << "\n"s;
        } else {
            std::cout << "a: "s << a << "\n"s;
        }
    }

    // 3. compare two string
    {    
        auto x = ""s;

        if ("a"s < "b"s){
            x = "b"s;
        } else {
            x = "a"s;
        }
        std::cout << "The bigger between a and b is "s << x << "\n"s;

        if ("aa"s < "ab"s){
            x = "ab"s;
        } else {
            x = "aa"s;
        }
        std::cout << "The bigger between aa and ab is "s << x << "\n"s;
    }

    // 4. bool
    {
        bool correct = true;
        bool wrong = false;

        std::cout << "correct = "s << correct << "\n"s; 
        std::cout << "wrong= "s << wrong << "\n"s; 

        std::cout << std::boolalpha;
        std::cout << "correct = "s << correct << "\n"s; 
        std::cout << "wrong= "s << wrong << "\n"s; 

        std::cout << std::noboolalpha;
        std::cout << "correct = "s << correct << "\n"s; 
        std::cout << "wrong= "s << wrong << "\n"s; 
    }

    // 5. minimum evaluation
    {
        auto a = [](){
            std::cout << "a\n"s;
            return false;
        };

        auto b = [](){
            std::cout << "b\n"s;
            return true;
        };

        if ( a() && b() ){
            std::cout << "a() && b() is ture.\n"s;
        }else{
            std::cout << "a() && b() is false.\n"s;
        }
 
        if ( b() || a() ){
            std::cout << "b() || a() is ture.\n"s;
        }else{
            std::cout << "b() || a() is false.\n"s;
        }
    }
    
    return EXIT_SUCCESS;
}
