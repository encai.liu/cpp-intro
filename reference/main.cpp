int main()
{
    // reference
    int a = 1;
    int b = a;      // b is another variable
    b = 3;          // a == 1, b == 3
    std::cout << "a = "s << a << ", b = "s << b << std::endl;

    int& c = a;     // c is a reference to a
    c = 5;          // a == 5, c == 5
    std::cout << "a = "s << a << ", c = "s << c << std::endl;

    // reference variable as a argument
    auto my_swap = [](auto& a, auto& b){    // Actually, there is a std::swap which can do this... 
        auto temp = a;
        a = b;
        b = temp;
    };

    a = 10;
    b = 20;
    std::cout << "a = "s << a << ", b = "s << b << std::endl;
    my_swap(a, b);
    std::cout << "a = "s << a << ", b = "s << b << std::endl;

    return EXIT_SUCCESS;
}
