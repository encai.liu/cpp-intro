int main()
{
    const int trial_count = 1000;

    std::mt19937 e;
    std::bernoulli_distribution d(32.0/100.0);

    std::array<int, 2> result{};
    for( int i=0; i!=trial_count; ++i){
        ++result[ d(e) ];
    }

    std::cout << "False: "sv << double(result[0]) / double(trial_count) * 100 << "%, "sv
              << "True: "sv  << double(result[1]) / double(trial_count) * 100 << "%\n"sv;

    return EXIT_SUCCESS;
}
