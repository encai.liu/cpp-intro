int main()
{
    std::mt19937 e;
    std::normal_distribution d(0.0, 1.0);

    std::cout << "d.mean(): "sv << d.mean() << std::endl;
    std::cout << "d.stddev(): "sv << d.stddev() << std::endl;

    for( int i=0; i!=20; ++i){
        std::cout << d(e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
