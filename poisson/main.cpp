
template <typename Engine>
auto traffic_accidents( unsigned int n, Engine & e)
{
    std::poisson_distribution d(n);
    return d(e);
}

int main()
{
    std::mt19937 e;

    // traffic_accidents
    for(auto i=0; i!=10; ++i)
    {
        std::cout << traffic_accidents(10, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}

