//auto for_each = [](auto first, auto last, auto f)
//{
//    for( auto iter = first; iter != last; ++iter){
//        f( *iter );
//    }
//};

int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5 };

    auto print_value = [](auto value){ std::cout << value << "\n"s; };
//    for_each(v.begin(), v.end(), [](auto value){ std::cout << value; } );
//    std::cout << "\n"s;

//    for_each(v.begin(), v.end(), [](auto value){ std::cout << 2*value; } ); 
//    std::cout << "\n"s;

    // we can use std::for_each, it not nessary for us to implement the function
    std::for_each(v.begin(), v.end(), [](auto value){ std::cout << 2*value; } ); 
    std::cout << "\n"s;

    std::for_each(v.begin(), v.end(), print_value);
    std::cout << "\n"s;

    std::for_each(v.begin(), v.end(), [](auto & value) { value = 2 * value; });

    std::for_each(v.begin(), v.end(), print_value);
    std::cout << "\n"s;

    return EXIT_SUCCESS;
}
