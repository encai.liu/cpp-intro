struct Point
{
    int x = 0;
    int y = 0;
};

struct Person
{
    std::string name;
    int age;
};

struct Fractional
{
    int num;
    int denom;

    // Constructor
    Fractional()
        : num(1), denom(1)
    { }

    // Constructor
    Fractional(int num)
        : num(num), denom(1)
    { }

    // Constructor
    Fractional(int num, int denom)
        : num(num), denom(denom)
    { }

    // Destrauctor
    ~Fractional()
    {
        std::cout << "Destructor called\n"s;
    }

    double value()
    {
        return static_cast<double>(num) / static_cast<double>(denom);
    }

    void set(int num_)
    {
        num = num_;
        denom = 1;
    }

    void set(int num_, int denom_)
    {
        num = num_;
        denom = denom_;
    }
};

Fractional add(Fractional &l, Fractional &r)
{
    if( l.denom == r.denom ){
        return Fractional{l.num + r.num, l.denom};
    }

    return Fractional{ l.num*r.denom + r.num*l.denom, l.denom*r.denom };
}

int main()
{
//    int x {};
//    int y {};
//    std::vector<Point> ps;
//
//    while(std::cin >> x >> y){
//        if ((x==0) && (y==0))
//            break;
//        std::cout << "x = "s << x << ", y = "s << y << std::endl;
//        ps.push_back( Point{x, y} );
//    }
//
//    for(auto iter = ps.begin(); iter != ps.end(); ++iter){
//        std::cout << iter->x << " "s << iter->y << std::endl;
//    }

    Person John;
    John.name = "John";
    John.age = 20;
    std::cout << "Name: "s << John.name << ", Age: "s << John.age << std::endl;

    Person Tom {"Tom", 21};
    std::cout << "Name: "s << Tom.name << ", Age: "s << Tom.age << std::endl;

    Fractional half {1, 2};
    std::cout << half.value() << std::endl;

    Fractional frac1, frac2;
    frac1.set(5);
    frac2.set(1, 4);
    std::cout << "frac1 = "s << frac1.value() << std::endl;
    std::cout << "frac2 = "s << frac2.value() << std::endl;

    std::cout << "frac2 + frac2 = "s << add(frac2, frac2).value() << std::endl;
    std::cout << "frac1 + frac2 = "s << add(frac1, frac2).value() << std::endl;

    Fractional frac3 = 1;
    Fractional frac4(3);
    Fractional frac5(3, 4);
    std::cout << "frac3 = "s << frac3.value() << std::endl;
    std::cout << "frac4 = "s << frac4.value() << std::endl;
    std::cout << "frac5 = "s << frac5.value() << std::endl;

    return EXIT_SUCCESS;
}
