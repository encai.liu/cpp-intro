int main()
{
    int * int_ptr = new int{123};
    int * int_array_ptr = new int[5]{1, 2, 3, 4, 5};

    std::cout << *int_ptr << std::endl;
    for(auto i=0; i<5; ++i)
        std::cout << *(int_array_ptr + i);
    std::cout << std::endl;

    delete int_ptr;
    delete []int_array_ptr;

    return EXIT_SUCCESS;
}
