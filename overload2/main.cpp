struct Fractional
{
    int num;
    int denom;

    // Constructor
    Fractional()
        : num(1), denom(1)
    { }

    // Constructor
    Fractional(int num)
        : num(num), denom(1)
    { }

    // Constructor
    Fractional(int num, int denom)
        : num(num), denom(denom)
    { }

    // Destrauctor
    ~Fractional()
    {
        std::cout << "Destructor called\n"s;
    }

    double value()
    {
        return static_cast<double>(num) / static_cast<double>(denom);
    }
 
    Fractional operator +(const Fractional &r)
    {
        if( denom == r.denom ){
            return Fractional{num + r.num, denom};
        }

        return Fractional{ num*r.denom + r.num*denom, denom*r.denom };
    }

    Fractional operator -(const Fractional &r)
    {
        if( denom == r.denom ){
            return Fractional{num - r.num, denom};
        }

        return Fractional{ num*r.denom - r.num*denom, denom*r.denom };
    }
    
    Fractional operator *(const Fractional &r)
    {
        return Fractional{ num*r.num, denom*r.denom };
    }

    Fractional operator /(const Fractional &r)
    {
        return Fractional{ num*r.denom, denom*r.num };
    }

    Fractional operator +()
    {
        return Fractional{ num, denom };
    }

    Fractional operator -()
    {
        return Fractional{ -num, denom };
    }
};


int main()
{
    Fractional frac1(1);
    Fractional frac2(3, 4);

    // binary operator
    Fractional frac3 = frac1 + frac2;
    std::cout << "frac(1) + frac(3, 4) = "s << frac3.value() << std::endl;

    Fractional frac4 = frac1 - frac2;
    std::cout << "frac(1) - frac(3, 4) = "s << frac4.value() << std::endl;

    Fractional frac5 = frac1 * frac2;
    std::cout << "frac(1) * frac(3, 4) = "s << frac5.value() << std::endl;

    Fractional frac6 = frac1 / frac2;
    std::cout << "frac(1) / frac(3, 4) = "s << frac6.value() << std::endl;

    // unary operator
    Fractional frac7 = +frac2;
    Fractional frac8 = -frac2;
    std::cout << "+frac(3, 4) = "s << frac7.value() << std::endl;
    std::cout << "-frac(3, 4) = "s << frac8.value() << std::endl;

    return EXIT_SUCCESS;
}
