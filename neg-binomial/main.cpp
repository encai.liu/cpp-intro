template <typename Engine>
auto count_n_coinflips(unsigned int n, Engine & e)
{
    std::negative_binomial_distribution d(n, 0.5);
    return d(e) + n; 
}

template <typename Engine>
auto count_n_rolls(unsigned int n, Engine & e)
{
    std::negative_binomial_distribution d(n, 1.0/6.0);
    return d(e) + 10;
}

template <typename Engine>
auto count_n_lootboxes(unsigned int n, Engine & e)
{
   std::negative_binomial_distribution d(n, 0.01);
    return d(e) + 10;
}

int main()
{

    std::mt19937 e;

    // coinflips
    for(auto i=0; i!=10; ++i)
    {
        std::cout << count_n_coinflips(10, e) << " "sv;
    }
    std::cout << std::endl;

    // rolls
    for(auto i=0; i!=10; ++i)
    {
        std::cout << count_n_rolls(10, e) << " "sv;
    }
    std::cout << std::endl;

    // lootboxes
    for(auto i=0; i!=10; ++i)
    {
        std::cout << count_n_lootboxes(10, e) << " "sv;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
