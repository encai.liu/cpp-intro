template <typename T, std::size_t N>
struct array
{
    T storage[N];

    // pointer
    using iterator = T *;

    iterator begin(){
        return &storage[0];
    }

    iterator end(){
        return begin() + N;
    }
};

int main(){
    array<int, 10> a { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    for(auto iter = a.begin(); iter != a.end(); iter++){
        std::cout << *iter;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
