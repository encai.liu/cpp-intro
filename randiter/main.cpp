template <typename T>
struct iota_iterator
{
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using reference = T &;
    using pointer = T *;
    using iterator_category = std::bidirectional_iterator_tag;

    // the value
    T value;

    // constructor
    iota_iterator( T value = 0 )
        : value (value)
    { }

    reference operator *() noexcept
    { return value; }

    const reference operator *() const noexcept
   { return value; }
 
    // operator -=
    iota_iterator & operator -=(difference_type n) noexcept
    {
        value -= n;
        return *this;
    }
 
    // operator -
    iota_iterator operator -(difference_type n) const
    {
        auto temp = *this;
        temp -= n;
        return *this;
    }
 
    // operator +=
    iota_iterator & operator +=(difference_type n) noexcept
    {
        value += n;
        return *this;
    }
 
    // operator +
    iota_iterator operator +(difference_type n) const
    {
        auto temp = *this;
        temp += n;
        return *this;
    }

    // operator -- (pre-)
    iota_iterator & operator --() noexcept
    {
        --value;
        return *this;
    }

    // operator -- (post-)
    iota_iterator operator --(int)
    {
        auto temp = *this;
        --*this;
        return temp;
    }

    // operator ++ (pre-)
    iota_iterator & operator ++() noexcept
    {
        ++value;
        return *this;
    }

    // operator ++ (post-)
    iota_iterator operator ++(int)
    {
        auto temp = *this;
        ++*this;
        return temp;
    }

    // operator ==
    bool operator ==(iota_iterator const & i) const noexcept
    {
        return (value == i.value);
    }

    // operator !=
    bool operator !=(iota_iterator const & i) const noexcept
    {
        return !(*this == i);
    }

    bool operator <(iota_iterator const & i) const noexcept
    { return value < i.value; }

    bool operator <=(iota_iterator const & i) const noexcept
    { return value <= i.value; }

    bool operator >(iota_iterator const & i) const noexcept
    { return value > i.value; }

    bool operator >=(iota_iterator const & i) const noexcept
    { return value >= i.value; }

    // difference_type
    difference_type operator - (iota_iterator const &i)
    {
        return value - i.value;
    }

};

template <typename T>
iota_iterator<T> operator +
(
    typename iota_iterator<T>::difference_type n,
    iota_iterator<T> const & i
)
{ return n + i; }

template <typename T>
iota_iterator<T> operator -
(
    typename iota_iterator<T>::difference_type n,
    iota_iterator<T> const & i
)
{ return n - i; }

template <typename T>
iota_iterator<T> operator + 
(
    iota_iterator<T> i,
    typename iota_iterator<T>::difference_type n
)
{ return i + n; }

template <typename T>
iota_iterator<T> operator - 
(
    iota_iterator<T> i,
    typename iota_iterator<T>::difference_type n
)
{ return i - n; }

int main()
{
    iota_iterator first(0), last(10);

    std::for_each(first, last, 
        [](auto i){ std::cout << i; });
    std::cout << std::endl;

    std::vector<int> v;
    std::copy(first, last, std::back_inserter(v));

    std::for_each(v.begin(), v.end(), 
        [](auto i){ std::cout << i; });
    std::cout << std::endl;

    iota_iterator iter(0);

    // increment
    std::cout << *iter++ << std::endl;  // 0
    std::cout << *iter << std::endl;    // 1
    std::cout << *++iter << std::endl;  // 2

    // decrement
    std::cout << *iter-- << std::endl;  // 2
    std::cout << *iter << std::endl;    // 1
    std::cout << *--iter << std::endl;  // 0

    // others
    std::cout << *iter + 10 << std::endl;    // 10
    std::cout << 10 + *iter << std::endl;    // 10
    std::cout << *iter - 10 << std::endl;    // -10
    std::cout << 10 - *iter << std::endl;    // 10

    std::cout << std::boolalpha;
    std::cout << (*iter != 0) << std::endl;  // false
    std::cout << (*iter == 0) << std::endl;  // true
    std::cout << (*iter > 0) << std::endl;   // false
    std::cout << (*iter < 1) << std::endl;   // true
    
    return EXIT_SUCCESS;
}
