template <typename T, typename Allocator=std::allocator<T> >
struct vector
{
public:
    // nested type name
    using value_type = T;
    using pointer = T *;
    using const_pointer = const pointer;
    using reference = value_type &;
    using const_reference = const value_type &;
    using allocator_type = Allocator;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using iterator = pointer;
    using const_iterator = const iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:
    // type name
    using traits = std::allocator_traits<allocator_type>;

    // data member
    pointer first = nullptr;
    pointer last = nullptr;
    pointer reserved_last = nullptr;
    allocator_type alloc;

    // helper function
    pointer allocate( size_type n )
    { return traits::allocate(alloc, n); }
    void deallocate( )
    { traits::deallocate(alloc, first, capacity() ); }

    void construct( pointer ptr )
    { traits::construct(alloc, ptr); }
    void construct( pointer ptr, const_reference value)
    { traits::construct(alloc, ptr, value); }
    void construct( pointer ptr, value_type && value )
    { traits::construct(alloc, ptr, std::move(value)); }
    void destroy( pointer ptr )
    { traits::destroy(alloc, ptr); }

    void destroy_until( reverse_iterator rend )
    {
        for( auto riter = rbegin(); riter != rend; ++riter, --last){
            destroy( &*riter );
        }
    }

    void clear() noexcept
    {
        destroy_until( rend() );
    }

public:
    // constructor
    vector( const allocator_type & alloc ) noexcept
        : alloc( alloc )
    { }

    vector ()
        : vector( allocator_type() )
    { }

    vector ( size_type n, const allocator_type & alloc = allocator_type() )
        : vector( alloc )
    { 
        resize( n );
    }

    vector ( size_type n, const_reference value, const allocator_type & alloc = allocator_type() )
        : vector( alloc )
    {
        resze( n, value ) ;
    }

    template <typename InIterator> 
    vector( InIterator first, InIterator last, const allocator_type & alloc = allocator_type() )
    {
        reserve( std::distance( first, last ) );
        for( auto iter = first; iter != last; ++iter ){
            push_back( *iter );
        }
    }

    vector(std::initializer_list<value_type> init, const allocator_type & alloc = allocator_type() )
        : vector( std::begin(init), std::end(init), alloc )
    { }

    // copy constructor
    vector(const vector & r)
        : alloc( traits::select_on_container_copy_construction( r.alloc ) )
    {
        // reserve the memory
        reserve( r.size() );

        // copy data
        for( auto dest = first, src = r.begin(), last = r.end();
                src != last; ++src, ++dest )
        {
            construct(dest, *src);
        }
        last = first + r.size();
    }

    // destructor
    ~vector()
    {
        // destroy all the element
        clear();

        // release the memory
        deallocate();
    }

    // copy
    vector & operator =(const vector & r)
    {
        // copy from itself, nothing needed
        if( this == &r )
            return *this;

        // copy from a vector with same size
        if( size() == r.size() ){
            std::copy( r.begin(), r.end(), begin() );
        }
        else{
            // the capacity is enough
            if( capacity() > r.size() ){
                std::copy( r.begin(), r.begin() + r.size(), begin() );
                std::size_t diff = size() - r.size();
                destroy_until( rbegin() + diff );

                //for( auto src_iter = r.begin() + r.size(), src_end = r.end();
                //        src_iter != src_end; ++src_iter, ++last)
                //{
                //    construct( last, *src_iter );
                //}
            }
            // the capacity is not enough
            else{
                // destroy all the element
                clear();

                // reserve the memory
                reserve( r.size() );

                // copy construct
                for( auto src_iter = r.begin(), src_end = r.end(), dest_iter = begin();
                    src_iter != src_end; ++src_iter, ++dest_iter, ++last )
                {
                    construct( dest_iter, *src_iter );
                }
            }
        }
        return *this;
    }

    // access to the element
    reference operator []( std::size_t i ) noexcept
    { return first[i]; }

    const_reference operator []( std::size_t i ) const noexcept
    { return first[i]; }

    reference at( std::size_t i ) 
    {
        if( i >= size() )
             throw std::out_of_range("index is out of range");

        return first[i]; 
    }

    const_reference at( std::size_t i ) const 
    { 
        if( i >= size() )
             throw std::out_of_range("index is out of range");

        return first[i]; 
    }

    // iterator access
    iterator begin() noexcept
    { return first; }

    iterator end() noexcept
    { return last; }

    iterator begin() const noexcept
    { return first; }

    iterator end() const noexcept
    { return last; }

    const_iterator cbegin() const noexcept
    { return first; }

    const_iterator cend() const noexcept
    { return last; }

    reverse_iterator rbegin() noexcept
    { return reverse_iterator{ last }; }

    reverse_iterator rend() noexcept
    { return reverse_iterator{ first }; }

    const_reverse_iterator crbegin() const noexcept
    { return reverse_iterator{ last }; }

    const_reverse_iterator crend() const noexcept
    { return reverse_iterator{ first }; }

    size_type size() const noexcept
    { return end() - begin(); }
    
    bool empty() const noexcept
    { return size() == 0; }

    size_type capacity() const noexcept
    { return reserved_last - first; } 

    reference front() noexcept
    { return first; }

    reference back() noexcept
    { return last - 1; }

    const_reference front() const noexcept
    { return first; }

    const_reference back() const noexcept
    { return last - 1; }

    pointer data() noexcept
    { return first; }

    const_pointer data() const noexcept
    { return first; }

    void reserve(size_type sz)
    {
        if ( sz < capacity() )
            return;

        // allocate the memory
        auto ptr = allocate( sz );

        // save the info of old storage 
        auto old_first = first;
        auto old_last = last;
        auto old_capacity = capacity();

        // use the new storage
        first = ptr;
        last = first;
        reserved_last = first + sz;

        ///////////////////////////////////////////////// compile error
        // // release the memory of old storage when exiting
        // std::scope_exit e(
        //     [&]{
        //         traits::deallocate( alloc, old_first, old_capacity );
        //     }
        // );
        ///////////////////////////////////////////////// compile error

        // copy the element from old storage to the new one
        for ( auto old_iter = old_first; old_iter != old_last; ++old_iter, ++last ){
            construct( last, std::move(*old_iter) );
        }

        // destroy the old storage
        for ( auto riter = reverse_iterator(old_last), rend = reverse_iterator(old_first);
            riter != rend; ++riter )
        {
            destroy(&*riter);
        }

        // release the memory
        traits::deallocate( alloc, old_first, old_capacity );
    }

    void resize( size_type sz )
    {
        if ( sz < size() ){
            auto diff = size() - sz;
            destroy_until( rbegin() + diff );
            last = first + sz;
        }
        else if ( sz > size() ){
            reserve( sz );
            for( ; last != reserved_last; ++last){
                construct( last );
            }
        }
    }
    
    void resize( size_type sz, value_type value )
    {
        if ( sz < size() ){
            auto diff = size() - sz;
            destroy_until( rbegin() + diff );
            last = first + sz;
        }
        else if ( sz > size() ){
            reserve( sz );
            for( ; last != reserved_last; ++last){
                construct( last, value );
            }
        }
    }

    void push_back( const_reference value )
    {
        if ( size() + 1 > capacity() ){
            auto c = size();

            if( c == 0 )
                c = 1;
            else
                c *= 2;

            reserve( c );
        }

        construct( last, value );
        ++last;
    }

    // release the unused memory
    void shrink_to_fit()
    {
        if( size() == capacity() ){
            // no redundancy, do nothing.
            return;
        }

        // allocate memory
        auto ptr = allocate( size() );

        // copy data
        auto current_size = size();
        for( auto raw_ptr = ptr, iter = begin(), iter_end = end();
                iter != iter_end; ++iter, ++raw_ptr )
        {
            construct( raw_ptr, *iter );
        }

        // release old memory
        clear();
        deallocate();

        // use the allocated the memory
        first = ptr;
        last = first + current_size;
        reserved_last = last;
    }
};

int main()
{
    vector<int> v1(10);

    std::cout << "v1.size() = " << v1.size() << "\n"s;
    std::cout << "v1.capacity() = " << v1.capacity() << "\n"s;

    // use vector like an array
    for( auto i=0; i<10; ++i )
        v1[i] = i;

    // iterator
    std::for_each( std::begin(v1), std::end(v1),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // reserve
    vector<int> v2 = { 1, 2, 3, 4, 5 };

    v2.reserve(3); // will be ignored
    std::cout << "v2.size() = " << v2.size() << "\n"s;          // 5
    std::cout << "v2.capacity() = " << v2.capacity() << "\n"s;  // 5

    v2.reserve(10);
    std::cout << "v2.size() = " << v2.size() << "\n"s;          // 5
    std::cout << "v2.capacity() = " << v2.capacity() << "\n"s;  // 10

    std::for_each( std::begin(v2), std::end(v2),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // resize
    vector<int> v3 = { 1, 2, 3 };
    v3.resize(5);
    std::cout << "v3.size() = " << v3.size() << "\n"s;          // 5
    std::cout << "v3.capacity() = " << v3.capacity() << "\n"s;  // 10

    std::for_each( std::begin(v3), std::end(v3),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    v3.resize(10, 6);
    std::cout << "v3.size() = " << v3.size() << "\n"s;          // 5
    std::cout << "v3.capacity() = " << v3.capacity() << "\n"s;  // 10

    std::for_each( std::begin(v3), std::end(v3),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    v3.resize(3);

    std::for_each( std::begin(v3), std::end(v3),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // push_back
    vector<int> v4;
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    v4.push_back(1);
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    v4.push_back(2);
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    v4.push_back(3);
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    v4.push_back(4);
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    v4.push_back(5);
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;

    std::for_each( std::begin(v4), std::end(v4),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // shrink_to_fit
    v4.shrink_to_fit();
    std::cout << "v4.size() = " << v4.size() << "\n"s;
    std::cout << "v4.capacity() = " << v4.capacity() << "\n"s;
    std::for_each( std::begin(v4), std::end(v4),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // init with iterator pair
    std::array<int, 5> a{1, 2, 3, 4, 5};
    vector<int> v5( std::begin(a), std::end(a) );
    std::cout << "v5.size() = " << v5.size() << "\n"s;
    std::cout << "v5.capacity() = " << v5.capacity() << "\n"s;
    std::for_each( std::begin(v5), std::end(v5),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // init with list
    vector<int> v6 = {61, 62, 63, 64, 65, 66};
    std::cout << "v6.size() = " << v6.size() << "\n"s;
    std::cout << "v6.capacity() = " << v6.capacity() << "\n"s;
    std::for_each( std::begin(v6), std::end(v6),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // copy construct
    vector<int> v7(v6);
    std::cout << "v7.size() = " << v7.size() << "\n"s;
    std::cout << "v7.capacity() = " << v7.capacity() << "\n"s;
    std::for_each( std::begin(v7), std::end(v7),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    // assignment copy
    vector<int> v8;
    std::cout << "v8.size() = " << v8.size() << "\n"s;
    std::cout << "v8.capacity() = " << v8.capacity() << "\n"s;
    v8 = v6;
    std::cout << "v8.size() = " << v8.size() << "\n"s;
    std::cout << "v8.capacity() = " << v8.capacity() << "\n"s;
    std::for_each( std::begin(v8), std::end(v8),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    v8 = v5;
    std::cout << "v8.size() = " << v8.size() << "\n"s;
    std::cout << "v8.capacity() = " << v8.capacity() << "\n"s;
    std::for_each( std::begin(v8), std::end(v8),
        []( auto x ){ std::cout << x << ", "; } );  // 1, 2, 3, 4, 5, 66, Not Correct!!!
    std::cout << std::endl; 

    vector<int> v9 = {1, 2, 3, 4, 5, 6};
    v8 = v9;
    std::cout << "v8.size() = " << v8.size() << "\n"s;
    std::cout << "v8.capacity() = " << v8.capacity() << "\n"s;
    std::for_each( std::begin(v8), std::end(v8),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 

    std::vector<int> v10 = {61, 62, 63, 64, 65, 66};
    std::vector<int> v11 = {1, 2, 3};
    std::cout << "v10.size() = " << v10.size() << "\n"s;
    std::cout << "v10.capacity() = " << v10.capacity() << "\n"s;
    std::for_each( std::begin(v10), std::end(v10),
        []( auto x ){ std::cout << x << ", "; } );
    std::cout << std::endl; 
    v10 = v11;
    std::cout << "v10.size() = " << v10.size() << "\n"s;
    std::cout << "v10.capacity() = " << v10.capacity() << "\n"s;
    std::for_each( std::begin(v10), std::end(v10),
        []( auto x ){ std::cout << x << ", "; } );  // 1, 2, 3,
    std::cout << std::endl; 

    return EXIT_SUCCESS;
}
