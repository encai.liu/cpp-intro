int main()
{
    // default_random_engine
    {
        std::default_random_engine e;

        // obtain a seed from the timer
        typedef std::chrono::high_resolution_clock myclock;
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        unsigned seed = d.count();

        std::cout << "e.min = "sv << e.min() << "\n"sv;
        std::cout << "e.max = "sv << e.max() << "\n"sv;

        e.seed(seed);
        for( int i=0; i<10; ++i ){
            std::cout << e() << "\n"sv;
        }
    }

    std::cout << "\n"sv;

    // mt19937
    {
        std::mt19937 e;

        // obtain a seed from the timer
        typedef std::chrono::high_resolution_clock myclock;
        myclock::time_point beginning = myclock::now();
        myclock::duration du = myclock::now() - beginning;
        unsigned seed = du.count();

        std::cout << "e.min = "sv << e.min() << "\n"sv;
        std::cout << "e.max = "sv << e.max() << "\n"sv;

        unsigned n {};
        // std::cout << "Please input the number:\n"sv;
        // std::cin >> n;
        n = 5;
        e.seed(seed);
        for( unsigned int i=0; i!=n; ++i ){
            std::cout << e() << "\n"sv;
        }

        std::uniform_int_distribution<int> d(1, 6);
        // std::cout << "Please input the number:\n"sv;
        // std::cin >> n;
        n = 5;
        for( unsigned int i=0; i!=n; ++i ){
            std::cout << d(e) << "\n"sv;
        }
    }

    // seed_seq
    {
        std::seed_seq s1{124};
        std::mt19937 e1(s1);
        std::cout << e1() << " with seed: 124.\n"sv;

        std::seed_seq s2{421};
        std::mt19937 e2(s2);
        std::cout << e2() << " with seed: 421.\n"sv;
    }

    // copy of a random engine
    {
        std::mt19937 e1;

        std::cout << e1() << " "sv << e1() << std::endl;

        std::mt19937 e2 = e1;  // Internal state will be copied
        std::cout << std::boolalpha;
        std::cout << (e1() == e2()) << "\n"sv;  // true
        std::cout << (e1() == e2()) << "\n"sv;  // true
    }

    // random device
    {
        std::random_device rd;
        for(auto i=0; i!=10; i++){
            std::cout << rd() << " "sv;
        }
        std::cout << "\n"sv;
    }

    // size of random engines
    {
        std::cout << "size of mt19937 random engine: "sv << sizeof(std::mt19937) << "\n"sv;
        std::cout << "size of default random engine: "sv << sizeof(std::default_random_engine) << "\n"sv;
    }

    // reset the state of distribution classes
    {
        std::uniform_int_distribution a(1, 6);
        std::uniform_int_distribution b(1, 6);

        std::mt19937 x;

        a(x);

        std::mt19937 y = x;

        // r1 may be not equal to r2 if random number is cached in a.
        // we can reset a to clear the state.
        a.reset();
        auto r1 = a(x);
        auto r2 = b(y);
        std::cout << "r1: "sv << r1 << ", r2: "sv << r2 << "\n"sv;
    }

    // uniform_real_distribution
    {
        std::mt19937 e;
        std::uniform_real_distribution rd(0.0, 1.0);

        std::cout << "rd.a: "sv << rd.a() << std::endl;
        std::cout << "rd.b: "sv << rd.b() << std::endl;

        for(auto i=0; i!=10; i++){
            std::cout << rd(e) << " "sv;
        }
        std::cout << "\n"sv;
    }

    // bernoulli distribution
    {
        std::mt19937 e;
        std::bernoulli_distribution d(0.2);
        std::cout << "p of the bernoulli distribution: "sv << d.p() << "\n"sv;

        for(auto i=0; i!=10; i++){
            std::cout << d(e) << " "sv;
        }
        std::cout << "\n"sv;
    }

    return EXIT_SUCCESS;
}
