auto is_all_of_odd = [](auto first, auto last)
{
    return std::all_of(first, last, [](auto value){ return (value%2 == 0); }); 
};

auto has_3 = [](auto first, auto last)
{
    return std::any_of(first, last, [](auto value){ return (value == 3); });
};

auto has_not_100 = [](auto first, auto last)
{
    return std::none_of(first, last, [](auto value){ return (value == 100); });
};

int main()
{
    std::vector<int> v = {1, 2, 3, 4, 5};

    std::cout << std::boolalpha;

    bool b_all_odd = is_all_of_odd(v.begin(), v.end());
    bool b_has_3 = has_3(v.begin(), v.end());
    bool b_has_not_100 = has_not_100(v.begin(), v.end());

    std::cout << "All of odd: "s << b_all_odd << "\n"s;
    std::cout << "Has 3: "s << b_has_3 << "\n"s;
    std::cout << "Has not 100: "s << b_has_not_100 << "\n"s;

    return EXIT_SUCCESS;
}
