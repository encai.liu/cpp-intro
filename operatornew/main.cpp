struct Logger
{
    std::string name;
    
    Logger( std::string name )
        : name( name )
    { std::cout << name << "is constructed.\n"s; }

    ~Logger()
    { std::cout << name << "is destructed.\n"s; }
};

int main()
{
    void *ptr = ::operator new ( sizeof(Logger) );
    Logger * logger_ptr = new (ptr) Logger("Alice"s);

    std::cout << "Some operation here.\n";

    logger_ptr->~Logger();
    ::operator delete(ptr);
}
