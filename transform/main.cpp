int main()
{
    auto print = [](auto v){
        for(auto iter = v.begin(); iter != v.end(); ++iter)
        {
            std::cout << *iter << " "s;
        }
        std::cout << std::endl;
    };

    std::vector<int> a = {1, 2, 3, 4, 5};
    std::vector<int> b(5);

    std::transform(a.begin(), a.end(), b.begin(),
            [](auto x){ return x*2; });
    print(b);

    std::transform(a.begin(), a.end(), b.begin(),
            [](auto x){ return x%3; });
    print(b);

    std::vector<bool> c(5);
    std::transform(a.begin(), a.end(), c.begin(),
            [](auto x){ return x<3; });
    std::cout << std::boolalpha;
    print(c);

    return EXIT_SUCCESS;
}
