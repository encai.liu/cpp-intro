struct funny_number
{
    int n;

    funny_number( int n = 0 )
        : n(n)
    { }

    funny_number( funny_number & source )
        : n(source.n)
    {
        source.n = 0;
    }
};

int main()
{
    funny_number a = 1;
    std::cout << "a: " << a.n << std::endl; // 1

    funny_number b = a;
    std::cout << "a: " << a.n << std::endl; // 0
    std::cout << "b: " << b.n << std::endl; // 1
    

    return EXIT_SUCCESS;
}
